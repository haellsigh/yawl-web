#**Yawl**
***

###**Yet Another Watch List**

The web frontend for the yawl api.

Run the api backend [here]()

<a href="http://i.imgur.com/4r7OyBO.png">
<img src="http://i.imgur.com/4r7OyBO.png" alt="Screenshot" style="width: 600px;"/>
</a>

***
##Delete bug
#####When you try to delete a ressource, you have to clear the cache or you won't see the updated content.
This is a known bug with should be fixed with eve 0.5.
You have to disable cache using chrome dev tools (F12) then the settings button [see here](https://github.com/nicolaiarocci/eve/issues/334) and [here for fix](http://stackoverflow.com/questions/5690269/disabling-chrome-cache-for-website-development)

***
##Commit to GitHub
    git commit -m "message"
    git push origin master