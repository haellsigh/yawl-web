'use strict';

/* Filters */

angular.module('yawlFilters', []).filter('animeSumCut', function() {
    return function(input) {
        var short = input.substr(0, 200);
        if (/^\S/.test(input.substr(200))) return short.replace(/\s+\S*$/, "");
        return short;
    };
});
