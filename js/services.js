'use strict';

/* Services */

var yawlServices = angular.module('yawlServices', ['ngResource']);

yawlServices.factory('yawlAnimes', ['$resource',

function($resource) {
    return $resource('http://yawl-c9-haellsigh.c9.io/animes/:identifier', {}, {
        get: {
            method: 'GET',
            params: {
                identifier: ''
            },
            isArray: false
        },
        post: {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            isArray: false
        },
        remove: {
            method: 'DELETE',
            isArray: false,
            params: {
                identifier: ''
            }
        },
        patch: {
            method: 'PATCH',
            headers: {'Content-Type': 'application/json'},
            isArray: false,
            params: {
                identifier: ''
            }
        }
    });
}]);
