'use strict';

/* App Module */

var yawlApp = angular.module('yawlApp', ['ngRoute', 'yawlControllers', 'yawlServices', 'yawlFilters', 'checklist-model', 'wu.masonry', 'angularFileUpload', 'ngImgur']);

yawlApp.config(['$routeProvider', '$locationProvider',
function($routeProvider, $locationProvider) {
    //$locationProvider.html5Mode(true);
    
    $routeProvider.
    when('/animes', {
        templateUrl: 'partials/animes/list.html',
        controller: 'AnimesListCtrl'
    }).
    when('/animes/edit/:identifier', {
        templateUrl: 'partials/animes/edit.html',
        controller: 'AnimesEditCtrl'
    }).
    when('/animes/create/:title', {
        templateUrl: 'partials/animes/create.html',
        controller: 'AnimesCreateCtrl'
    }).
    when('/animes/create/', {
        templateUrl: 'partials/animes/create.html',
        controller: 'AnimesCreateCtrl'
    }).
    when('/animes/details/:identifier', {
        templateUrl: 'partials/animes/detail.html',
        controller: 'AnimesDetailCtrl'
    }).
    otherwise({
        redirectTo: '/animes'
    });
}]);
