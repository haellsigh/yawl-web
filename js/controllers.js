'use strict';

/* Controllers */

var yawlControllers = angular.module('yawlControllers', ['ui.bootstrap']);

yawlControllers.controller('AnimesListCtrl', ['$scope', '$sce', 'yawlAnimes',

    function($scope, $sce, yawlAnimes) {
        //Main function for /animes
        $scope.alerts = [];

        $scope.refresh = yawlAnimes.get({}, function(data) {
            $scope.animes = data._items;
        });

        $scope.refresh;

        $scope.deliberatelyTrustDangerousSnippet = function(htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.deleteAnime = function(_id) {
            console.log("Deleting anime with id: " + _id);
            yawlAnimes.remove({
                    identifier: _id
                }, function(success) {
                    $scope.refresh
                    $scope.alerts.push({
                        type: 'alert-success',
                        msg: '<strong>Success!</strong> Anime was successfully deleted.'
                    });
                },

                function(error) {
                    $scope.refresh
                    $scope.alerts.push({
                        type: 'alert-danger',
                        msg: '<strong>Error!</strong> Couldn\'t delete the anime.'
                    });
                });
        };
    }
]);

yawlControllers.controller('AnimesEditCtrl', ['$scope', '$routeParams', 'yawlAnimes',

    function($scope, $routeParams, yawlAnimes) {
        //Main function for /animes/edit/:identifier
        $scope.anime = {};
        $scope.alerts = [];
        $scope.allowedStatus = [{
            status: "finished airing",
            name: "Finished airing"
        }, {
            status: "currently airing",
            name: "Currently airing"
        }, {
            status: "not yet aired",
            name: "Not yet aired"
        }];
        $scope.availableGenres = ["Action", "Adventure", "Cars", "Comedy", "Dementia", "Demons", "Drama", "Ecchi", "Fantasy", "Game", "Harem", "Hentai", "Historical", "Horror", "Josei", "Kids", "Magic", "Martial Arts", "Mecha", "Military", "Music", "Mystery", "Parody", "Police", "Psychological", "Romance", "Samurai", "School", "Sci-Fi", "Seinen", "Shoujo", "Shoujo Ai", "Shounen", "Shounen Ai", "Slice of Life", "Space", "Sports", "Super Power", "Supernatural", "Thriller", "Vampire", "Yaoi", "Yuri"];
        $scope.anime.status = $scope.allowedStatus[0];

        $scope.$watch('anime.title', function(newValue, oldValue) {
            if ($scope.anime.title !== "" && $scope.anime.title) {
                $scope.anime.identifier = $scope.anime.title.replace(/(\W| )/g, "").toLowerCase();
            }
        });

        yawlAnimes.get({
            identifier: $routeParams.identifier
        }, function(data) {
            delete data._updated;
            delete data._links;
            delete data._created;
            $scope.anime = data;
            console.log($scope.anime.status);
            if ($scope.anime.status == 'finished airing') {
                $scope.anime.status = $scope.allowedStatus[0];
            }
            else if ($scope.anime.status == 'currently airing') {
                $scope.anime.status = $scope.allowedStatus[1];
            }
            else {
                $scope.anime.status = $scope.allowedStatus[2];
            }
        });

        $scope.editAnime = function() {
            $scope.anime.status = $scope.anime.status.status;
            if ($scope.anime._id) {
                $scope.savedId = $scope.anime._id;
                delete $scope.anime._id;
            }

            yawlAnimes.patch({
                    identifier: $scope.savedId
                },
                $scope.anime,
                function(success) {
                    if (success._status === "OK") {
                        $scope.alerts.push({
                            type: 'alert-success',
                            msg: '<strong>Success!</strong> \"' + $scope.anime.title + '\" was successfully added.'
                        });
                    }

                },
                function(error) {
                    for (var key in error.data._issues) {
                        $scope.alerts.push({
                            type: 'alert-danger',
                            msg: '<strong>Error!</strong> ' + error.data._issues[key]
                        });
                    }
                });
        };

    }
]);

yawlControllers.controller('AnimesDetailCtrl', ['$scope', '$routeParams', 'yawlAnimes',

    function($scope, $routeParams, yawlAnimes) {
        //Main function for /animes/:identifier
        yawlAnimes.get({
            identifier: $routeParams.identifier
        }, function(data) {
            $scope.anime = data;
        });

    }
]);

yawlControllers.controller('AnimesCreateCtrl', ['$scope', '$sce', '$routeParams', 'yawlAnimes', '$upload',

    function($scope, $sce, $routeParams, yawlAnimes, imgur) {
        //Main function for /animes/:identifier
        $scope.anime = {};
        $scope.anime.genres = [];
        $scope.anime.title = "";
        $scope.alerts = [];
        $scope.allowedStatus = [{
            status: "finished airing",
            name: "Finished airing"
        }, {
            status: "currently airing",
            name: "Currently airing"
        }, {
            status: "not yet aired",
            name: "Not yet aired"
        }];
        $scope.anime.status = $scope.allowedStatus[0];

        $scope.availableGenres = ["Action", "Adventure", "Cars", "Comedy", "Dementia", "Demons", "Drama", "Ecchi", "Fantasy", "Game", "Harem", "Hentai", "Historical", "Horror", "Josei", "Kids", "Magic", "Martial Arts", "Mecha", "Military", "Music", "Mystery", "Parody", "Police", "Psychological", "Romance", "Samurai", "School", "Sci-Fi", "Seinen", "Shoujo", "Shoujo Ai", "Shounen", "Shounen Ai", "Slice of Life", "Space", "Sports", "Super Power", "Supernatural", "Thriller", "Vampire", "Yaoi", "Yuri"];

        $scope.$watch('anime.title', function(newValue, oldValue) {
            if ($scope.anime.title !== "" && $scope.anime.title) {
                $scope.anime.identifier = $scope.anime.title.replace(/(\W| )/g, "").toLowerCase();
            }
        });

        $scope.closeAlert = function(index) {
            $scope.alerts.splice(index, 1);
        };

        $scope.deliberatelyTrustDangerousSnippet = function(htmlCode) {
            return $sce.trustAsHtml(htmlCode);
        };

        $scope.anime.title = $routeParams.title;

        $scope.upload = function(toUpload) {
            imgur.upload(toUpload).then(function then(model) {
                console.log('Image link: ' + model.link);
            });
        }

        $scope.createAnime = function() {
            console.log($scope.anime);
            if ($scope.formCreateAnime.title.$valid === true && $scope.formCreateAnime.episodes.$valid === true) {
                $scope.anime.status = $scope.anime.status.status;
                yawlAnimes.post($scope.anime, function(success) {

                    if (success._status === "OK") {
                        $scope.alerts.push({
                            type: 'alert-success',
                            msg: '<strong>Success!</strong> \"' + $scope.anime.title + '\" was successfully added.'
                        });
                    }
                }, function(error) {
                    for (var key in error.data._issues) {
                        $scope.alerts.push({
                            type: 'alert-danger',
                            msg: '<strong>Error!</strong> ' + error.data._issues[key]
                        });
                    }
                });
            }
        };
    }
]);